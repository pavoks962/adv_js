// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний 
// список публікацій. 
// Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі 
// публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), 
// та включати заголовок, 
// текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі 
// сторінки. 
// При натисканні на неї необхідно надіслати DELETE запит на адресу 
// https://ajax.test-danit.com/api/json/posts/${postId}.
// Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити 
// зі сторінки, використовуючи 
// JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна 
// знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, 
// не будуть там збережені.
// Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. 
// При необхідності ви можете додавати також інші класи.
// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. 
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, 
// в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно
// надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху 
// сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації 
// користувача з id: 1.

// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно 
// надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.

const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

// Cтворюємо клас для відмалювання карток
class Card {
    constructor(name, nickname, email, city, title, body, id, postId) {
        this.name = name;
        this.nickname = nickname;
        this.email = email;
        this.city = city;
        this.title = title;
        this.body = body;
        this.id = id;
        this.postId = postId;
        this.newTitle;
        this.newBody;

        this.itemNews = document.createElement('div');
        this.buttonDelete = document.createElement('button');
        this.buttonEdit = document.createElement('button');
        this.buttonSave = document.createElement('button');
    }

    createElement() {
        this.itemNews.innerHTML = `<div class="news-item__user">
            <h1 class="user-name">${this.name}</h1>
            <p class="user-nick">${this.nickname}</p>
            <address class="user-email">${this.email}</address>
        </div>
        <div class="news-item__city">${this.city}</div>
        <div class="news-item__post">
                    <h2>${this.title}</h2>
                    <p class="news-post">${this.body}</p>
                </div>`

        this.itemNews.className = `news-item news-item${this.id}`;

// Функціонал кнопки "Видалити" --------------------------------------

        this.buttonDelete.innerText = 'Delete...';
        this.buttonDelete.className = 'button button--delete';
        this.itemNews.prepend(this.buttonDelete);

        this.buttonDelete.addEventListener('click', ev => {
            axios.get(`${urlPosts}/${this.postId}`)
            .then(({status}) => {
                if(status === 200) {
                    this.itemNews.remove();
                }
            })
        })
// Функціонал кнеопки "Редагувати"  -----------------------------------
        this.buttonEdit.innerText = '   Edit...';
        this.buttonEdit.className = 'button button--edit';
        this.itemNews.append(this.buttonEdit);

        this.buttonEdit.addEventListener('click', ev => {
            this.showModalEdit();
            this.saveEditPost();
        })
    }
// Створення модального вікна для редагування картки --------------------
    showModalEdit () {
        this.modalEdit = document.createElement('div');
        this.modalEdit.className = 'news-form-edit';
        this.modalEdit.innerHTML = `<h3>Edit news</h3>
        <input type="text" name="title" value="${this.title}"><br>
        <input type="text" name="body" value="${this.body}"><br>`;
    
    this.itemNews.append(this.modalEdit);
    this.buttonSave.innerText = 'Save...';
    this.buttonSave.className = 'button button--save';
    this.modalEdit.append(this.buttonSave);
    }
// Збереження внесених змін і закриття модально вікна редагування  -------
    saveEditPost() {
        this.buttonSave.addEventListener('click', ev => {
            this.newTitle = this.modalEdit.querySelector('input[name="title"]').value;
            this.newBody = this.modalEdit.querySelector('input[name="body"]').value;
     
                        axios.put(`${urlPosts}/${this.postId}`, {title: this.newTitle, body: this.newBody})
                        .then(({request}) => {
                            if (request.status === 200) {
                                this.title = this.newTitle;
                                this.body = this.newBody;

                                this.itemNews.querySelector('h2').textContent = this.title;
                                this.itemNews.querySelector('.news-post').textContent = this.body;
                                this.modalEdit.remove()
                            }
                        })
                        .catch(error => console.error('Error:', error));
        })
    }
    
    render(selector) {
        this.createElement();  
        document.querySelector(`.${selector}`).prepend(this.itemNews);
    }
}


// Отримання даних з API та відмалювання карток --------------------
axios.get(urlUsers)
    .then(({ data }) => {

        data.forEach(({ name, username, email, address, id }) => {
            axios.get(`https://ajax.test-danit.com/api/json/users/${id}/posts`)
            .then(({data}) => {
                data.forEach(({title, body, userId, id}) => {
                    new Card(name, username, email, address.city, title, body, userId, id).render('news-container');
                })
            })
            .catch(error => console.error("ERROR:", error))
        })
    })
    .then((resp) => {document.querySelector(`.preloader`).classList.add('loaded')})
    .catch(err => console.error("ERROR:", err))

// Додавання нової картки -------------------------------------------

document.querySelector('.button--add').addEventListener('click', ev => {
    ev.target.style.display = 'none';
    document.querySelector('.news-form').style.display = 'block';
})

document.querySelector('.news-form').addEventListener('submit', ev => {
    ev.preventDefault();
    
    const user = {};
    ev.target.querySelectorAll('input').forEach((input) => {
        user[input.name] = input.value 
    })

    axios.post(`https://ajax.test-danit.com/api/json/posts`, user)
        .then(({ data }) => {
            console.log(user)
            let {UserId, title, body} = data;
            axios.get(urlUsers)
                .then(({ data }) =>
                data.forEach(({name, username, email, address, id}) => {
                   if (id == UserId) {
                        new Card(name, username, email, address.city, title, body, UserId).render('news-container');
                   }
                })             
                )              
        })
        .catch(err => console.error("ERROR:", err))
})



//--------------------------------------------------------------------
// class Post {
//     constructor(title, body, userId) {
//         this.title = title;
//         this.body = body;
//         this.userId = userId;
//     }

//     createElement() {
//         this.newsPost = `<div class="news-item__post">
//                     <h2>${this.title}</h2>
//                     <p class="news-item__post">${this.body}</p>
//                 </div>`
//     }

//     render(selector) {
//         this.createElement();
//         document.querySelector(`.${selector}`).insertAdjacentHTML('beforeend', this.newsPost);
//     }
// }

// class Card {
//     constructor(name, nickname, email, city, id) {
//         this.name = name;
//         this.nickname = nickname;
//         this.email = email;
//         this.city = city;
//         this.id = id;
//     }

//     createElement() {
//         this.itemNews = `<div class="news-item news-item${this.id}">
//         <button class="button-delete">Delete...</button>
//         <div class="news-item__user">
//             <h1 class="user-name">${this.name}</h1>
//             <p class="user-nick">${this.nickname}</p>
//             <address class="user-email">${this.email}</address>
//         </div>
//         <div class="news-item__city">${this.city}</div>
//     </div>`
//     }

//     render(selector) {
//         this.createElement();
//         document.querySelector(`.${selector}`).insertAdjacentHTML('beforeend', this.itemNews);
//     }
// }

// axios.get('https://ajax.test-danit.com/api/json/users')
//     .then(({ data }) => {
//         console.log(data)
//         data.forEach(({ name, username, email, address, id }) => {
//             new Card(name, username, email, address.city, id).render('news-container');
//         })
//     })

// axios.get('https://ajax.test-danit.com/api/json/posts')
//     .then(({ data }) => {
//         data.forEach(({ title, body, userId }) => {

//             new Post(title, body).render(`news-item${userId}`)
//         })
//     })

