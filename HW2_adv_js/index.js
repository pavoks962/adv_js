"use strict"

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  const container = document.getElementById('root');
  const list = document.createElement("ul");
  list.innerText = "Books list";
  container.append(list);

class Books {
    constructor(author, name, price) {
        if (!author) {
            throw new InvalidItemBook(name, 'author');
        }
        if (!name) {
            throw new InvalidItemBook(name, 'name');
        }
        if (!price) {
            throw new InvalidItemBook(name, 'price');
        }
        this.author = author;
        this.name = name;
        this.price = price;
    }

    createElement () {
        this.listItem = document.createElement('li');
        this.listItem.innerText = `"${this.name}" - Автор: ${this.author}; ціна: ${this.price}.`
    }

    render (selector) {   
        this.createElement();  
        document.querySelector(selector).append(this.listItem);
    }
}

class InvalidItemBook extends Error {
    constructor (name, prop) {
        super();
        this.name = 'InvalidItemBook';
        this.message = `Книга: "${name}". Властивість:  ${prop} не вказана`;
    }
}

books.forEach( el => {
    try {
        new Books(el.author, el.name, el.price).render('ul');
    } catch (err) {
        if (err.name === 'InvalidItemBook') {
            console.error(err.message);
        } else {
            throw err;
        }
    }
})


