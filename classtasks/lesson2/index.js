// "use strict"
/**
 * @return {boolean}
 */
const getRandomChance = () => Math.random() < 0.5;
// const container = document.querySelector(selector);

class Animal {
    constructor (name) {
        this.name = name;
    }
    div = null;
    btn = null;
    num = 0;
    // get name () {
    //     if (this._name.split(',').length < 3) {
    //         alert ("Довжина імені має бути не менше 3-х символів")
    //     } return this._name;
    // }

    // set name (name) {
    //     return this._name = name;
    // }


render(selector) {
    this.createDomElements();
    const container = document.querySelector(selector);
    console.log(container);
    container.append(this.div);
}

createDomElements() {
  this.div = document.createElement('div');
  this.div.className = "animal";
  this.div.innerHTML = `<span>${this.name}</span>`;
  this.btn = document.createElement('button');
  this.btn.innerText = 'Move';
  this.div.append(this.btn);
  this.btn.addEventListener('click', () => {
    this.move()
  }); 
}

move() {
    this.div.style.transform = `translateX(${this.num += 20}px)`;
}
}

class Dog extends Animal {
    createDomElements () {
        super.createDomElements();
        this.div.classList.add('dog');
    }

    say () {
        this.div.classList.add('dog-say');
        setTimeout(() => {
            this.div.classList.remove('dog-say')
        }, 200)
    }

    move () {
        super.move();
        if (getRandomChance()) {
            super.move();
            this.say();
        }
    }
}

class Cat extends Animal {
    createDomElements () {
        super.createDomElements();
        this.div.classList.add('cat');
    }

    say () {
        this.div.classList.add('cat-say');
        setTimeout(() => {
            this.div.classList.remove('cat-say')
        }, 200)

    }

    move () {
        if (getRandomChance()) {
            this.say();
        } else {
            super.move();
        }
    }
}

class Snake extends Animal {
    constructor (name, type) {
        super(name);
        this.type = type;
    }

    createDomElements () {
        super.createDomElements();
        this.div.classList.add('snake');
    }

    // say () {
    //     if (getRandomChance() && this.type === 'poison') {
           
    //          this.div.classList.remove('snake')
    //     }
    // }
    
    move () {
        this.div.style.transform = `translateX(${this.num += 5}px)`
    }
}

const dog = new Dog('Dog');
dog.render('.container');

const cat = new Cat('Cat');
cat.render('.container');

const snake = new Snake('Snake', 'poison');
snake.render('.container');

