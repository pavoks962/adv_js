"use strict"

// По кліку на кнопку зчитувати країну, яка обрана в селекті і відправляти GET запит
// на http://universities.hipolabs.com/search?country=${country}.
// Малювати в ul посилання на сайт кожного університету.
// Поки йде завантаження даних - показуйте прелоадер (можна просто слово Loading...)

// fetch('http://universities.hipolabs.com/search?country=${country}')
// .then(res => res.json())
// .then(data => console.log(data))

class Country {
    constructor (country, name, url) {
        this.country = country;
        this.name = name;
        this.url = url;
        this.ul = null;
        this.list = null;
    }

    createElement () {
        this.ul = document.createElement('ul');
        this.ul.innerText = `${this.country}`;
        this.list = document.createElement('li');
        this.ul.innerText = `${this.name}: ${this.url}`;
    }

    render () {
        this.createElement();
        document.body.append(this.ul);
        this.ul.append(this.list);
        
    }
}


const btn = document.querySelector('button');

btn.addEventListener('click', (ev) => {
    const select = document.querySelector('select');
    if (document.querySelector('ul')) {
        document.querySelector('ul').remove() 
    }

axios.get(`http://universities.hipolabs.com/search?country=${select.value}`)
.then(response => {

    let countryUniv = response.data;
    countryUniv.forEach(({country, name, web_pages}) => {
        new Country(country, name, web_pages).render()
    });
    
    console.log(countryUniv);



})
// .catch(error => console.error(error));

})

