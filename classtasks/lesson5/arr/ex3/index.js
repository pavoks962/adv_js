/*
Напишіть функцію, яка створює об'єкт.
Як аргументи вона приймає в себе ім'я, прізвище, та перелік рядків формату "ім'яВластивості: значення". Їх може бути багато.

Приклад роботи:

const user = createObject("Jhon", "Johnson", "age: 31", "wife: Marta", "dog: Bob");
=>
user = {
    name: "Jhon",
    lastName: "Johnson",
    age: "31",
    wife: "Marta",
    dog: "Bob",
}
 */

const createObject = (name, lastName, ...rest) => {
    const object = {name, lastName};
    rest.forEach(el => {
        const [key, value] = (el.split(': '));
        object[key] = value;
    })
    console.log(object);
}

createObject("Shanon", "Kerr", "age: 24", "cat: Honey", "city: LA");
createObject("Kaison", "Dalby");
createObject("Rebekka", "Solomon", "country: France", "city: Marseilles");
