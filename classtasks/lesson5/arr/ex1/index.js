/*
Напишіть код, який створює змінні з ім'ям людини, а також з професією, виводить запис типу "userName is userProfession"; */

'use strict'
const user1 = 'Cristian Stuart; developer';
const user2 = 'Archibald Robbins; seaman';
const user3 = 'Zach Dunlap; lion hunter';
const user4 = 'Uwais Johnston; circus artist';

const createUser = (user) => {
   const [userName, userProfession] = user.split('; ');
   return `${userName} is ${userProfession}`;
}

console.log(createUser(user1));
console.log(createUser(user2));
console.log(createUser(user3));
console.log(createUser(user4));