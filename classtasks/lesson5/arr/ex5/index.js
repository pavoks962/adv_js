/*
При натисканні на кнопку з класом button виведіть у p з id user-number номер елемента у списку меню, на який ми клацнули.
Використовуйте отримані знання:)
  */

const button = document.querySelectorAll('button');
const userNumber = document.querySelector('#user-number');

button.forEach((el, index) => {
    el.addEventListener('click', (ev) => {
        userNumber.innerText = `${index+1}`;
        console.log(index+1);
    })
})

console.log(button)


