/*
1. Виведіть у параграф з id = href рядок url поточної сторінки у браузері;
2. Напишіть функціонал перезавантаження сторінки на кліку на кнопку з id = reload;
*/

const hrefP = document.querySelector('#href');

const {href} = location;

hrefP.innerText = href;

document.querySelector('#reload').addEventListener('click', ()=> {
    location.reload();
})

console.log(location)



