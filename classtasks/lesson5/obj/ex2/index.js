/*
По кліку на кнопку, виведете alert з текстом з input
*/

const input = document.querySelector('#text');
const button = document.querySelector('#alert-text');

button.addEventListener('click', () => {
    const {value} = input;
    alert(value)
})

