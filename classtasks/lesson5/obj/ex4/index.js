/*
Напишіть функцію, яка приймає 2 об'єкти - резюме і вакансію,
та повертає відсоток збігу необхідних навичок (скіллів).

Навичка збігається якщо ім'я скіла збігається з імен
у вакансії та і необхідний досвід <= досвіду людини в цій навичці
  */

const resume = {
    name: "Илья",
    lastName: "Куликов",
    age: 29,
    city: "Киев",
    skills: [
        {
            name: "Vanilla JS",
            practice: 5
        },
        {
            name: "ES6",
            practice: 3
        },
        {
            name: "React + Redux",
            practice: 1
        },
        {
            name: "HTML4",
            practice: 6
        },
        {
            name: "CSS2",
            practice: 6
        }
    ]
};


const vacancy = {
    company: "SoftServe",
    location: "Киев",
    skills: [
        {
            name: "Vanilla JS",
            experience: 3
        },
        {
            name: "ES6",
            experience: 2
        },
        {
            name: "React + Redux",
            experience: 2
        },
        {
            name: "HTML4",
            experience: 2
        },
        {
            name: "CSS2",
            experience: 2
        },
        {
            name: "HTML5",
            experience: 2
        },
        {
            name: "CSS3",
            experience: 2
        },
        {
            name: "AJAX",
            experience: 2
        },
        {
            name: "Webpack",
            experience: 2
        }
    ]
};

const getRezult = (resume, vacancy) => {
    const resumeSkills = resume.skills;
    const vacancySkills = vacancy.skills;

    let skills = 0;

    for (let i = 0; i < resumeSkills.length; i++) {
        const resumeSkill = resumeSkills[i];
        const vacancySkill = vacancySkills.find(skills => skills.name === resumeSkill.name);
        console.log(vacancySkill);

        if (vacancySkill.experience <= resumeSkill.practice) {
            skills++;
            console.log(skills)
        }
    }
    const skillsPercentage = (skills / vacancySkills.length) * 100;
    return skillsPercentage;
};

const rezult = getRezult(resume, vacancy);
console.log(rezult);