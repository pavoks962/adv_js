"use strict"
/*
    напишите объекту btn метод render, который рисует кнопку в переданном DOM-элементе кнопку с текстом, храняющимся в свойстве
    btn.text, и вешает на нее обработчик click, который при клике меняет текст на храняющийся в свойстве secondText
    */
    // <!--<button id="click-me">Click me</button>-->

    function Btn (text, secondText, selector) {
        this.text = text;
        this.secondText = secondText;
        this.selector = selector;
        // render: function (id) {

        // }
    }

Btn.prototype.render  = function() {
    const btnClick = document.querySelector(this.selector);

    const btn = document.createElement('button');
    btn.id = 'click-me';
    btn.innerText = this.text;
    
    document.querySelector('body').append(btn);

    btnClick.addEventListener('click', (ev) => {
        // ev.target.innerText = this.secondText
        console.log(btnClick);
    });
   
}


const btnNew = new Btn("Click-me", "Eat me", "#click-me").render();

    // Btn.render("Click-me", "Eat me", "#click-me");

    console.log(btnNew);