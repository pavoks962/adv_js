"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return (this._name);
    }

    set name(name) {
        return `Name: ${this._name}`;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        return this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        return this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary () {
        return (this._salary * 3);
    }
}

const qa = new Programmer("John", 35, 800, ['uk', 'pl', 'fr', 'es']);
const frontend = new Programmer("Bob", 27, 1200, ['uk', 'en']);
const ux = new Programmer("Sem", 24, 1000, ['uk','pl', 'sl']);

console.log(qa);
console.log(qa.salary);
console.log(frontend);
console.log(frontend.salary);
console.log(ux);
console.log(ux.salary);

  