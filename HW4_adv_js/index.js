// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з 
// властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, 
// назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. 
// Бажано знайти варіант на чистому CSS без використання JavaScript.

const url = 'https://ajax.test-danit.com/api/swapi/films';
const loader = document.querySelectorAll('.loader');

// Створюємо клас для карток з фільмами.
class Card {
    constructor(name, episode, desc, characters) {
        this.name = name;
        this.episode = episode;
        this.desc = desc;
        this.characters = characters;
    }

    createlement() {
        this.filmItem = `
        <div class="films">
            <div class="wrapper_title">
                    <span>Episode: ${this.episode}</span>
                    <h3>Title: "${this.name}"</h3>
            </div>
            <p>${this.desc}</p>
            <ul class="list list-${this.episode}">Characters:</ul>
            <div class="loader-${this.episode}">
                </div>
        </div>`
       
    }

    render (selector) {
        this.createlement();
        document.querySelector(`.${selector}`).insertAdjacentHTML('beforeend', this.filmItem);
    }
} 

// Створюємо клас для карток з персонажами.

class Character {
    constructor (name) {
        this.name = name;
    }

    createlement () {
        this.ItemList = `<li>${this.name}</li>`;
    }

    render (selector) {
        this.createlement();
        document.querySelector(`.${selector}`).insertAdjacentHTML('beforeend', this.ItemList);     
    }
}

fetch(url).then((resp) => resp.json())
    .then((data) => {

        data.forEach(({ name, episodeId, openingCrawl, characters }) => {
            new Card(name, episodeId, openingCrawl).render('container');
            document.querySelector(`.loader-${episodeId}`).classList.add('loader');

            characters.map((urlCharacter) =>
                fetch(urlCharacter).then((resp) => resp.json())
                    .then((dataCharacter) =>

                        new Character(dataCharacter.name).render(`list-${episodeId}`))
                    .then(data => { document.querySelector(`.loader-${episodeId}`).classList.remove('loader') }));
        })
    }).catch((error) => console.log('Дані відсутні'))


 
      
  

