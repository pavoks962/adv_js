
// Написати програму "Я тебе знайду по IP"
// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, 
// отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про 
// фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, 
// регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const urlIp = `https://api.ipify.org/?format=json`;

const getLocation = async () => {
    try {
        const {data} = await axios.get(urlIp);

        await axios.get(`http://ip-api.com/json/${data.ip}`)
        .then(({data}) => {

            const userLocation = document.createElement('div');
            userLocation.className = 'user-wrapper';
            userLocation.innerHTML = `
            <span class="continent">Continent: ${data.timezone}</span>
            <span class="country">Country: ${data.country}</span>
            <span class="region">Region: ${data.regionName}</span>
            <span class="city">City: ${data.city}</span>
            <span class="area">Area: </span>`

            document.querySelector('.container').append(userLocation)
        })
    } catch (err) {
        console.error('Error:', err)
    }
}

document.querySelector('.button').addEventListener('click', e => {
    getLocation();
})


