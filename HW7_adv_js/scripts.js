// Створити поле 10*10 за допомогою елемента <table>.
// Суть гри: будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір. 
// Користувач повинен протягом відведеного часу встигнути клацнути на зафарбовану комірку. 
// Якщо користувач встиг це зробити, вона забарвлюється зелений колір, користувач отримує 1 очко. 
// Якщо не встиг – вона забарвлюється у червоний колір, комп'ютер отримує 1 очко.
// Гра триває доти, доки половина комірок на полі не будуть пофарбовані у зелений чи червоний колір.
//  Як тільки це станеться, той гравець (людина чи комп'ютер), чиїх комірок на полі більше, перемагає.

// Гра повинна мати три рівні складності, що вибираються перед стартом гри:
// Легкий – нова комірка підсвічується кожні 1.5 секунди;
// Середній - нова комірка підсвічується раз на секунду;
// Важкий - нова комірка підсвічується кожні півсекунди.

// Після закінчення гри вивести на екран повідомлення про те, хто переміг.
// Після закінчення гри має бути можливість змінити рівень складності та розпочати нову гру.
// Використовувати функціонал ООП під час написання програми.
// За бажанням, замість фарбування комірок кольором, можна вставляти туди картинки.


const buttonsLevel = document.querySelectorAll('.button');

let interval;

    buttonsLevel.forEach(btn => {
      
      btn.addEventListener('click', (ev) => {
        const btnActive = document.querySelector('.button-active');
        if (btnActive) {btnActive.classList.remove('button-active')};
        ev.target.classList.toggle('button-active');
        const level = ev.target.dataset.level;
  
      if (level === 'easy') {
        interval = 1500;
      } else if (level === 'medium') {
        interval = 1000;
      } else if (level === 'hard') {
        interval = 500;
      } 
      })
    });

    const startButton = document.querySelector('.button-start');
    startButton.addEventListener('click', ev => {
      
      new Game(interval);
    })

class Table {
  constructor () {
    this.row = null;
    this.col = null;
}

  render (selector)  {
    for (let i = 0; i < 10; i++) {
      this.row = document.createElement('tr');
      document.querySelector(`${selector}`).append(this.row);

      for (let j = 1; j <= 10; j++) {
        this.col = document.createElement('td');
        this.row.append(this.col);
      }
    }
  }
}

new Table().render('table');

class Game {
  constructor (interval) {
    this.i = null;
    this.activeCell = null;
    this.interval = interval;
    
    this.countGamer = document.querySelector('.count__gamer');
    this.countComputer = document.querySelector('.count__computer');
    this.startGame();   
  }
  
  startGame () {
    this.ballGamer = 0;
    this.ballComputer = 0;
    this.summ = 0;
    this.randomColor();
   
  }

  createElement () {
    this.countGamer.innerText = this.ballGamer;
    this.countComputer.innerText = this.ballComputer;
  }

  randomColor () {
    this.cells = [...document.querySelectorAll('td')];
    this.i = Math.floor(Math.random() * this.cells.length);
    this.cell = this.cells[this.i];
    this.cell.classList.add('active');
    this.summ = this.ballGamer + this.ballComputer;
    
    this.timeInterval();
    this.addListener();
    this.createElement();
    this.endGame();
  }

  timeInterval() {
    this.time = setTimeout(() => {
    this.cell.classList.remove('active');
    this.cell.classList.add('loss'); 
    this.ballComputer += 1;
    this.randomColor();  
   }, this.interval);
  }

  addListener() {
    this.activeCell = document.querySelector('.active');
    this.activeCell.addEventListener('click', (ev) => {
      ev.target.classList.add('touch');
      this.ballGamer += 1;
      clearTimeout(this.time);
      setTimeout(() => {
        ev.target.classList.remove('active');
        this.randomColor();
      }, 300);
    })
  }

  endGame() {
    if (this.summ >= this.cells.length / 10) {
      clearTimeout(this.time);
      this.ballGamer > this.ballComputer ? alert('The Gamer is winner') : alert('The Computer is winner');
      this.cells.forEach(el => {
        el.classList.remove('active');
        el.classList.remove('touch');
        el.classList.remove('loss');

      })
    }
    }
} 
